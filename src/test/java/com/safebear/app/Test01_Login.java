package com.safebear.app;

import org.junit.Test;
import sun.awt.windows.WEmbeddedFrame;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 06/02/2018.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {

        //Step1 confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());

        //Step2 click on the login link and the login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 login with valid credentials
        assertTrue(loginPage.login(this.userPage, "testuser", "testing"));
    }
}
