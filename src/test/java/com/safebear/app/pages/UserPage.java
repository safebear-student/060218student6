package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 06/02/2018.
 */
public class UserPage {

    WebDriver driver;

    public UserPage(WebDriver _driver) {

        this.driver = _driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {

        return driver.getTitle().startsWith("Logged In");
    }
}
